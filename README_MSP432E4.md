# MSP432E4 LaunchPad Set Up
## Read This First
The instructions in this file apply specifically to a user who has directly cloned or downloaded the "aws-iot-device-sdk-embedded-c" repository. Please note that it is highly recommended to install the MSP432E4 Plugin for AWS IoT, versus cloning or downloading this repository directly.  The repository by itself only provides basic build support for the subscribe/publish example, with makefiles that use the TI compiler.  Full example support for TI, GCC and IAR is provided via the Plugin installation.

If you have installed the Plugin for AWS IoT (i.e. did not clone/download the repository), then please refer to the Quick Start Guide that is included with your Plugin installation (you should be able to access this via the "docs" folder of the Plugin for AWS IoT installation). The Quick Start Guide provides more accurate instructions that apply specifically to the Plugin installation.

## Overview
This file contains the following information:
- How to set up an account with Amazon Web Services
- How to build a cloned git repository of the AWS SDK and the subscribe/publish example for the following devices:
    - [TI MSP432E4 LaunchPad](http://www.ti.com/tool/MSP-EXP432E401Y)
- How to run the subscribe/publish example.

## Table of Contents
- [Required Hardware](#RequiredHW)
- [TI Software Installation](#TI-SW)
- [Build Environment Set Up](#Enviro-Setup)
- [AWS IoT Installation And Set Up](#AWS-Install)
- [Update The AWS Configuration File](#Update-AWSCONFIG)
- [Example Pre-Build Steps](#PreBuild-EXAMPLE)
- [Update The Example To Use The Certificates](#Update-CERTS)
- [Building The Example Applications](#Build-EXAMPLE)
- [Setting Up Code Composer Studio Before Running The Examples](#Setup-CCS)
- [Running An Example](#Run-EXAMPLE)

<a name="RequiredHW"></a>
## Required Hardware
One of the following devices are required for this SDK:
- [TI MSP432E4 LaunchPad](http://www.ti.com/tool/MSP-EXP432E401Y)

Please ensure that your device has been updated with the latest firmware and or service pack (instructions for updating the firmware and/or service pack are included with the SimpleLink MSP432E4 SDK installation).

<a name="TI-SW"></a>
## TI Software Installation
The following TI software products must be installed in order to build and run the example applications. While not strictly required, we recommend that you install these products into a common directory and that you use directory names without any whitespace. This documentation assumes that you haved installed everything into a directory named `C:/ti`.

- Install [Code Composer Studio (CCS) IDE, v10.0 or compatible](http://processors.wiki.ti.com/index.php/Download_CCS)
- Install [TI SimpleLink MSP432E4 Software Development Kit (SDK), v4.10 or compatible](http://www.ti.com/tool/simplelink-msp432-sdk)

<a name="Enviro-Setup"></a>
## Build Environment Set Up
### Updating Paths To Product Dependencies
1. Edit the `products.mak` file in `<AWS_INSTALL_DIR>/` using your favorite text editor.
1. Update the variables `SIMPLELINK_MSP432E4_SDK_INSTALL_DIR` and `XDC_INSTALL_DIR` variables to point to the installed locations of these products.
1. The variable `TI_ARM_CODEGEN_INSTALL_DIR` should point to the installation location of the TI ARM compiler in your CCS installation.
1. The variable `SYSCONFIG_TOOL` should point to the installation location of the SysConfig tool in your CCS installation.
1. Note that the variable `SIMPLELINK_CC32XX_SDK_INSTALL_DIR` *does not* need to be updated, as it is used for a different hardware platform.
1. After modification, these variable definitions should look similar to the following if you are working in Windows. (Windows users: note the use of "/" in the path).
    ```
    SIMPLELINK_MSP432E4_SDK_INSTALL_DIR = C:/ti/simplelink_msp432e4_sdk_4_10_xx_xx
    XDC_INSTALL_DIR = C:/ti/xdctools_3_61_00_16_core
    TI_ARM_CODEGEN_INSTALL_DIR = C:/ti/ccs1000/ccs/tools/compiler/ti-cgt-arm_20.2.0.LTS
    SYSCONFIG_TOOL = C:/ti/ccs1000/ccs/utils/sysconfig_1.4.0/sysconfig_cli.bat
    ```

### Notes On The gmake Utiltity
When building an application for the first time, the TI ARM compiler first builds the C runtime library, which requires that `gmake` is on the environment path. It is therefore recommended that the `gmake` tool be installed on your machine and added to the environment path.

Alternatively, the XDCtools product contains `gmake`, which can be utilized by adding the install location of XDCtools to the environment path. But note, it is recommended that the XDCtools is removed from the path after the inital step of building the C runtime libraries is complete. This is to avoid conflicts when you use newer versions of XDCtools.

For detailed information about building TI ARM C runtime library, please read the [Mklib wiki](http://processors.wiki.ti.com/index.php/Mklib).

<a name="AWS-Install"></a>
## AWS IoT Installation And Set Up
### AWS IoT SDK Installation
This procedure assumes you have already cloned or downloaded the AWS SDK repository onto your development machine. These instructions refer to the folder that contains the AWS SDK repository on your local machine as `<AWS_INSTALL_DIR>`.

<a name="AWS-setup"></a>
### AWS IoT Developer Set Up
In order to run the example applications, it is necessary to complete the steps of the [AWS IoT Developer Guide](http://docs.aws.amazon.com/iot/latest/developerguide/iot-gs.html).  In particular, the following steps of the guide must be completed:

1. [Sign in to the AWS IoT Console](http://docs.aws.amazon.com/iot/latest/developerguide/iot-console-signin.html)
1. [Register a Device in the Thing Registry](http://docs.aws.amazon.com/iot/latest/developerguide/register-device.html)
    - Make sure you choose to download the certificate files, as these will be needed when setting up the examples.

For more details on AWS IoT, please refer to the [What Is AWS IoT](http://docs.aws.amazon.com/iot/latest/developerguide/what-is-aws-iot.html) page.

<a name="PreBuild-EXAMPLE"></a>
## Example Pre-Build Steps

Complete the following steps in order to build the `subscribe_publish_sample` application for the MSP432E4 device:

<a name="Update-AWSCONFIG"></a>
## Update The AWS Configuration File
Before you set up the AWS configuration file, you'll need to retrieve your AWS IoT's custom endpoint URL.

Navigate to your AWS IoT dashboard and click Settings on the sidebar. Your custom endpoint will be posted at the top of this page. Copy this value and save it somewhere safe, you'll need it in the next step.  

1. In a text editor, open the `aws_iot_config.h` file from the directory `<AWS_INSTALL_DIR>/samples/tirtos/subscribe_publish_sample/msp432e4`
1. Update the value of the macro `AWS_IOT_MQTT_HOST` with your AWS IoT's custom endpoint URL. It will have a format similar to the following: `<random-string>-ats.iot.us-east-2.amazonaws.com`
1. Update the value of the macro `AWS_IOT_MQTT_CLIENT_ID` to a unique name for your device
1. Update the value of the macro `AWS_IOT_MY_THING_NAME` with the name of the thing you created during the steps on the [Register a Device in the Thing Registry](http://docs.aws.amazon.com/iot/latest/developerguide/register-device.html) web page
1. The certificate file variables `AWS_IOT_ROOT_CA_FILENAME`, `AWS_IOT_CERTIFICATE_FILENAME`, and `AWS_IOT_PRIVATE_KEY_FILENAME` can be left as the default values.

<a name="Update-CERTS"></a>
## Update The Example To Use The Certificates
The certificate files used by the examples need to be obtained from the AWS IoT Console. If you have not already done so, follow the steps for [AWS IoT Developer Setup](#AWS-setup) to create and download the certificates for your device.

1. Open the file `certs.c` from the directory `<AWS_INSTALL_DIR>samples/tirtos/subscribe_publish_sample/msp432e4`

1. Search for "USER STEP" and update the CA root certificate string, the client certificate string, and the client (private) key string. These should be extracted from certificate (.pem) files downloaded from AWS. Each line of the certificate must be updated to end with the character sequence `\r\n`.
A typical string would be similar to the following (example) format. Be sure to update the array using the contents of your actual root CA:
    ```
    unsigned char root_ca_pem[] =
    "-----BEGIN CERTIFICATE-----\r\n"
    "ABCD1234hxS39SFVT43STY#fdsiEIOFiu2=y!8cZxzXMh+12wDCy21h+8vJsDh4\r\n"
    <...>
    "rzcuMZt6oPq6YJMtP3kSaik+bWyzRkdMOPGVL4TiPLAuyAZP1NXl\r\n";
    "-----END CERTIFICATE-----";
    ```

<a name="Build-EXAMPLE"></a>
## Building The Example Applications

On the command line, enter the following commands to build the application:

    cd <AWS_INSTALL_DIR>/samples/tirtos/subscribe_publish_sample/msp432e4
    C:/ti/xdctools_3_61_00_16_core/gmake all

<a name="Setup-CCS"></a>
## Setting Up Code Composer Studio Before Running The Examples
1. Plug the MSP432E4 LaunchPad into a USB port on your PC

1. Open a serial session to the appropriate COM port with the following settings:

    ```
    Baudrate:     115200
    Data bits:       8
    Stop bits:       1
    Parity:       None
    Flow Control: None
    ```

1. Plug an Ethernet cable into the MSP432E4 LaunchPad

1. Open Code Composer Studio.

1. Open the Target Configurations View - Window -> Show View -> Target Configurations

1. Right-click on User Defined. Select New Target Configuration.

1. Use `MSP432E4.ccxml` as "File name". Hit Finish.

1. In the Basic window, select "Texas Instruments XDS110 USB Debug Probe" as the "Connection", and then type "MSP432E401Y" in the "Board or Device" text field. Check the box next to "MSP432E401Y". Hit Save.

1. Right-click "MSP432E4.ccxml" in the Target Configurations View. Hit Launch Selected Configuration.

1. Under the Debug View, right-click on "Texas Instruments XDS110 USB Debug Probe_0/Cortex_M4_0". Select "Connect Target".

<a name="Run-EXAMPLE"></a>
## Running An Example
1. Ensure the MSP432E4 is connected in CCS.

1. Select Run menu -> Load -> Load Program..., and browse to the file `subscribe_publish_sample.out` in `<AWS_INSTALL_DIR>/samples/tirtos/subscribe_publish_sample/msp432e4`. Hit OK. This will load the program onto the board.

1. Run the application by pressing F8. The output will appear in your serial terminal session:

    ```
    Service Status: DHCPC    : Enabled  :          : 000

    Service Status: DHCPC    : Enabled  : Running  : 000

    Network Added:
    If-1:192.168.1.109

    Service Status: DHCPC    : Enabled  : Running  : 017

    startSNTP: Current time: Tue Aug  7 19:05:49 2018

    AWS IoT SDK Version 3.0.1-

    Connecting...

    Subscribing...

    -->sleep

    Subscribe callback

    sdkTest/sub     hello from SDK QOS0 : 0

    Subscribe callback

    sdkTest/sub     hello from SDK QOS1 : 1

    -->sleep
    ```
