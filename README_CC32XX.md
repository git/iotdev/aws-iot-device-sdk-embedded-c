# CC32XX LaunchPad Set Up
## Read This First
The instructions in this file apply specifically to a user who has directly cloned or downloaded the "aws-iot-device-sdk-embedded-c" repository. Please note that it is highly recommended to install the CC32XX Plugin for AWS IoT, versus cloning or downloading this repository directly.  The repository by itself only provides basic build support for the subscribe/publish example, with makefiles that use the TI compiler.  Full example support for TI, GCC and IAR is provided via the Plugin installation.

If you have installed the Plugin for AWS IoT (i.e. did not clone/download the repository), then please refer to the Quick Start Guide that is included with your Plugin installation (you should be able to access this via the "docs" folder of the Plugin for AWS IoT installation). The Quick Start Guide provides more accurate instructions that apply specifically to the Plugin installation.

## Overview
This file contains the following information:
- How to set up an account with Amazon Web Services
- How to build a cloned git repository of the AWS SDK and the subscribe/publish example for the following devices:
    - [TI CC3220SF LaunchPad](http://www.ti.com/tool/cc3220sf-launchxl)
    - [TI CC3220S LaunchPad](http://www.ti.com/tool/cc3220s-launchxl)
    - [TI CC3235SF LaunchPad](http://www.ti.com/tool/cc3235sf-launchxl)
    - [TI CC3235S LaunchPad](http://www.ti.com/tool/cc3235s-launchxl)
- How to run the subscribe/publish example.

## Table of Contents
- [Required Hardware](#RequiredHW)
- [TI Software Installation](#TI-SW)
- [Build Environment Set Up](#Enviro-Setup)
- [AWS IoT Installation And Set Up](#AWS-Install)
- [Update The AWS Configuration File](#Update-AWSCONFIG)
- [Example Pre-Build Steps](#PreBuild-EXAMPLE)
- [Update The Example To Use The Certificates](#Update-CERTS)
- [Update The Example With Your Wi-Fi Access Point Information](#Update-WIFI)
- [Building The Example Applications](#Build-EXAMPLE)
- [Setting Up Code Composer Studio Before Running The Examples](#Setup-CCS)
- [Running An Example](#Run-EXAMPLE)

<a name="RequiredHW"></a>
## Required Hardware
One of the following devices are required for this SDK:
- [CC3220SF LaunchPad](http://www.ti.com/tool/cc3220sf-launchxl)
- [CC3220S LaunchPad](http://www.ti.com/tool/cc3220s-launchxl)
- [CC3235SF LaunchPad](http://www.ti.com/tool/cc3235sf-launchxl)
- [CC3235S LaunchPad](http://www.ti.com/tool/cc3235s-launchxl)

Please ensure that your device has been updated with the latest firmware and or service pack (instructions for updating the firmware and/or service pack are included with the SimpleLink CC32XX SDK installation).

<a name="TI-SW"></a>
## TI Software Installation
The following TI software products must be installed in order to build and run the example applications. While not strictly required, we recommend that you install these products into a common directory and that you use directory names without any whitespace. This documentation assumes that you haved installed everything into a directory named `C:/ti`.

- Install [Code Composer Studio (CCS) IDE, v10.1 or compatible](http://processors.wiki.ti.com/index.php/Download_CCS)
- Install [TI SimpleLink Wi-Fi CC32XX Software Development Kit (SDK), v4.30 or compatible](http://www.ti.com/tool/simplelink-cc3220-sdk)
- Install [Uniflash Tool](http://www.ti.com/tool/uniflash)

<a name="Enviro-Setup"></a>
## Build Environment Set Up
### Updating Paths To Product Dependencies
1. Edit the `products.mak` file in `<AWS_INSTALL_DIR>/` using your favorite text editor.
1. Update the variables `SIMPLELINK_CC32XX_SDK_INSTALL_DIR` and `XDC_INSTALL_DIR` variables to point to the installed locations of these products.
1. The variable `TI_ARM_CODEGEN_INSTALL_DIR` should point to the installation location of the TI ARM compiler in your CCS installation.
1. The variable `SYSCONFIG_TOOL` should point to the installation location of the SysConfig tool in your CCS installation.
1. Note that the variable `SIMPLELINK_MSP432E4_SDK_INSTALL_DIR` *does not* need to be updated, as it is used for a different hardware platform.
1. After modification, these variable definitions should look similar to the following if you are working in Windows. (Windows users: note the use of "/" in the path).
    ```
    SIMPLELINK_CC32XX_SDK_INSTALL_DIR = C:/ti/simplelink_cc32xx_sdk_4_30_xx_xx
    XDC_INSTALL_DIR = C:/ti/xdctools_3_61_02_27_core
    TI_ARM_CODEGEN_INSTALL_DIR = C:/ti/ccs1011/ccs/tools/compiler/ti-cgt-arm_20.2.1.LTS
    SYSCONFIG_TOOL = C:/ti/ccs1011/ccs/utils/sysconfig_1.6.0/sysconfig_cli.bat
    ```

### Notes On The gmake Utiltity
When building an application for the first time, the TI ARM compiler first builds the C runtime library, which requires that `gmake` is on the environment path. It is therefore recommended that the `gmake` tool be installed on your machine and added to the environment path.

Alternatively, the XDCtools product contains `gmake`, which can be utilized by adding the install location of XDCtools to the environment path. But note, it is recommended that the XDCtools is removed from the path after the inital step of building the C runtime libraries is complete. This is to avoid conflicts when you use newer versions of XDCtools.

For detailed information about building TI ARM C runtime library, please read the [Mklib wiki](http://processors.wiki.ti.com/index.php/Mklib).

<a name="AWS-Install"></a>
## AWS IoT Installation And Set Up
### AWS IoT SDK Installation
This procedure assumes you have already cloned or downloaded the AWS SDK repository onto your development machine. These instructions refer to the folder that contains the AWS SDK repository on your local machine as `<AWS_INSTALL_DIR>`.

<a name="AWS-setup"></a>
### AWS IoT Developer Set Up
In order to run the example applications, it is necessary to complete the steps of the [AWS IoT Developer Guide](http://docs.aws.amazon.com/iot/latest/developerguide/iot-gs.html).  In particular, the following steps of the guide must be completed:

1. [Sign in to the AWS IoT Console](http://docs.aws.amazon.com/iot/latest/developerguide/iot-console-signin.html)
1. [Register a Device in the Thing Registry](http://docs.aws.amazon.com/iot/latest/developerguide/register-device.html)
    - Make sure you choose to download the certificate files, as these will be needed when setting up the examples.
    - Note: Due to the way the CC32XX device verifies the certificate chain, you need to **use a different root CA** than the one provided by AWS. This is because the Amazon root CA certificate is cross-signed with the Starfield Class 2 CA root certificate, therefore the CC32XX requires the Starfield Class 2 CA root certificate instead. You can download the root CA from [here](https://ssl-ccp.secureserver.net/repository/sf-class2-root.crt).

For more details on AWS IoT, please refer to the [What Is AWS IoT](http://docs.aws.amazon.com/iot/latest/developerguide/what-is-aws-iot.html) page.

<a name="PreBuild-EXAMPLE"></a>
## Example Pre-Build Steps

Complete the following steps in order to build the `subscribe_publish_sample` application for the CC3220SF device (the same procedure applies to any CC32XX device, as well. Just replace "3220sf" with the device number of your platform (e.g. If you are using the CC3235SF board, replace "3220sf" with "3235sf". For the CC3220S, replace "3220sf" with "3220s", etc.)):

<a name="Update-AWSCONFIG"></a>
## Update The AWS Configuration File
Before you set up the AWS configuration file, you'll need to retrieve your AWS IoT's custom endpoint URL.

Navigate to your AWS IoT dashboard and click Settings on the sidebar. Your custom endpoint will be posted at the top of this page. Copy this value and save it somewhere safe, you'll need it in the next step.  

1. In a text editor, open the `aws_iot_config.h` file from the directory `<AWS_INSTALL_DIR>/samples/tirtos/subscribe_publish_sample/cc3220sf`
1. Update the value of the macro `AWS_IOT_MQTT_HOST` with your AWS IoT's custom endpoint URL. It will have a format similar to the following: `<random-string>-ats.iot.us-east-2.amazonaws.com`
1. Update the value of the macro `AWS_IOT_MQTT_CLIENT_ID` to a unique name for your device
1. Update the value of the macro `AWS_IOT_MY_THING_NAME` with the name of the thing you created during the steps on the [Register a Device in the Thing Registry](http://docs.aws.amazon.com/iot/latest/developerguide/register-device.html) web page
1. The certificate file variables `AWS_IOT_ROOT_CA_FILENAME`, `AWS_IOT_CERTIFICATE_FILENAME`, and `AWS_IOT_PRIVATE_KEY_FILENAME` can be updated to specify the location to where the files are written in flash on the device. You can use the default values, if desired.

<a name="Update-CERTS"></a>
## Update The Example To Use The Certificates
The certificate files used by the examples need to be obtained from the AWS IoT Console. If you have not already done so, follow the steps for [AWS IoT Developer Setup](#AWS-setup) to create and download the certificates for your device.

The Uniflash tool is used to flash the CA root certificate, the client certificate, and the client (private) key. Consult [UniFlash ImageCreator Basics](http://dev.ti.com/tirex/explore/node?node=AH9w8QwBrpt-RLlf.VRAvg__fc2e6sr__LATEST), for instructions on how to setup the UniFlash ImageCreator tool and use it to add user files such as certificates.

Before flashing the certificates make sure to rename the files to match the value of the variables `AWS_IOT_ROOT_CA_FILENAME`, `AWS_IOT_CERTIFICATE_FILENAME`, and `AWS_IOT_PRIVATE_KEY_FILENAME` set in the AWS configuration file and add any folders needed to construct the correct path.

<a name="Update-WIFI"></a>
## Update The Example With Your Wi-Fi Access Point Information

1. Open the file `wificonfig.h` from the directory `<AWS_INSTALL_DIR>samples/tirtos/subscribe_publish_sample/cc3220sf`. Search for "USER STEP" and update the Wi-Fi SSID and SECURITY_KEY macros.

<a name="Build-EXAMPLE"></a>
## Building The Example Applications

On the command line, enter the following commands to build the application:

    cd <AWS_INSTALL_DIR>/samples/tirtos/subscribe_publish_sample/cc3220sf
    C:/ti/xdctools_3_61_02_27_core/gmake all

<a name="Setup-CCS"></a>
## Setting Up Code Composer Studio Before Running The Examples
1. Plug the CC32XX LaunchPad into a USB port on your PC

1. Open a serial session to the appropriate COM port with the following settings:

    ```
    Baudrate:     115200
    Data bits:       8
    Stop bits:       1
    Parity:       None
    Flow Control: None
    ```

1. Open Code Composer Studio.

1. Open the Target Configurations View - Window -> Show View -> Target Configurations

1. Right-click on User Defined. Select New Target Configuration.

1. Use `CC32XX.ccxml` as "File name". Hit Finish.

1. In the Basic window, select "Texas Instruments XDS110 USB Debug Probe" as the "Connection", and then type "CC32" in the "Board or Device" text field. A filtered list of all CC32XX devices will appear.

1. Check the box next to the device entry that matches the CC32XX LaunchPad that you are using. Hit Save. (Following suit with the previous steps, for the CC3220SF LaunchPad, select "CC3220SF" here)

1. Right-click "CC32XX.ccxml" in the Target Configurations View. Hit Launch Selected Configuration.

1. Under the Debug View, right-click on "Texas Instruments XDS110 USB Debug Probe_0/Cortex_M4_0". Select "Connect Target".

<a name="Run-EXAMPLE"></a>
## Running An Example
1. Ensure the CC32XX is connected in CCS.

1. Select Run menu -> Load -> Load Program..., and browse to the file `subscribe_publish_sample.out` in `<AWS_INSTALL_DIR>/samples/tirtos/subscribe_publish_sample/cc3220sf`. Hit OK. This will load the program onto the board.

1. Run the application by pressing F8. The output will appear in your serial terminal session:

    ```
    startSNTP: Current time: Tue Aug  7 19:28:11 2018

    CC32XX has connected to AP and acquired an IP address.

    IP Address: 192.168.1.131

    AWS IoT SDK Version 3.0.1-

    Connecting...

    Subscribing...

    -->sleep

    Subscribe callback

    sdkTest/sub     hello from SDK QOS1 : 1

    Subscribe callback

    sdkTest/sub     hello from SDK QOS0 : 0

    -->sleep
    ```
