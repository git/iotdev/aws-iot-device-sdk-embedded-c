# Copyright 2015-2019 Texas Instruments Incorporated. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License.
# A copy of the License is located at
#
# http://aws.amazon.com/apache2.0
#
# or in the "license" file accompanying this file. This file is distributed
# on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# express or implied. See the License for the specific language governing
# permissions and limitations under the License.
#
#
#  ======== products.mak ========
#

###### User Step: Update install paths to SDK(s), XDCtools, and Compiler ######
SIMPLELINK_CC32XX_SDK_INSTALL_DIR = /path/to/cc3220_sdk_installation
SIMPLELINK_MSP432E4_SDK_INSTALL_DIR = /path/to/msp432e4_sdk_installation

XDC_INSTALL_DIR = /path/to/xdctools_installation
TI_ARM_CODEGEN_INSTALL_DIR = /path/to/TI_ARM_compiler
SYSCONFIG_TOOL = /path/to/sysconfig_tool
